#!/bin/bash

###########################
#
# importdb.sh
# Import database to current drupal installation
# Run as ./importdb.sh "database-dump.sql.gz"
#
#
###########################

if [ -z "$1" ]; then
  echo "Please add the database file name after the command. Example './importdb.sh database-dump.sql.gz'."
  exit
else
  gunzip < $1 | drush sqlc
  echo "Database imported."
fi
exit


