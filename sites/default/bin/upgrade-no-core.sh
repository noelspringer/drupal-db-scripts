#!/bin/bash

#######
#
#  update-no-core.sh
#  sites/default/bin/update-no-core.sh
#
#  Drupal update script for contributed modules.
#  This version need to be run from within the sites/default/bin/ directory.
#  Uses Drush etc. to safely make an archive backup
#  and update contributed modules.
#  See update-core.sh to upgrade Drupal core between minor versions.
#  NOTE: Run update-no-core.sh first before upgrading Drupal core.
#
#  Use this script at your own risk.
#
#######

DATE=$(date +%y%m%d.%H.%M.%S)
DBNAME=`drush sql-connect | awk '{print $2}' | cut -b 1-11 --complement`
BASH="/bin/bash"
ECHO="/bin/echo"
VERSION=`drush core-status drupal-version | awk '{print $4}'` # Core is not being updated so VERSION can be set once.

if [ ! -f config.cfg ]; then
  echo ""
  echo "config.cfg is not found."
  echo "Copy or move config.cfg.dist to config.cfg and edit in the production database values."
  echo ""
  exit
elif [ -f config.cfg ]; then
. ./config.cfg
  if \
    [ -z $PROD_USER ]  ||\
    [ -z $PROD_PASSW ] ||\
    [ -z $PROD_DB ]    ||\
    [ -z $PROD_HOST ]; then
    echo ""
    echo "Please enter production database values in config.cfg."
    echo ""
  elif \
    [ -z $SITE ]  ||\
    [ -z $DRUPAL_ROOT_DEV ] ||\
    [ -z $BACKUP_DIR ]; then
    echo ""
    echo "Please enter directory values in config.cfg."
    echo ""
    exit
  fi
fi

# Put site into maintenance mode
drush vset --exact maintenance_mode 1 -y

# Clear cache
drush cache-clear all

# Backup this development db first
${ECHO}
${ECHO} "Backup this development db first to ${DRUPAL_ROOT_DEV}/sites/default/db"
${BASH} ${DRUPAL_ROOT_DEV}/sites/default/bin/dump-this-db.sh "d${VERSION}-pre-import-prod"

# Import production database to this development database
${ECHO}
${ECHO} "Import production database to this ${DBNAME}"

${BASH} ${DRUPAL_ROOT_DEV}/sites/default/bin/import-prod2this.sh

if [ ! -d "${BACKUP_DIR}/${SITE}" ]; then
  mkdir -p "${BACKUP_DIR}/${SITE}"
fi

${ECHO} "Import done."
${ECHO}

# Create full file system and db backup
${ECHO} "Create full file system and db backup."

if [ ! -d "${BACKUP_DIR}/${SITE}" ]; then
  mkdir -p "${BACKUP_DIR}/${SITE}"
fi

drush archive-dump --destination="${BACKUP_DIR}/${SITE}/${DATE}_${SITE}_d${VERSION}.tar" -y

${ECHO} "Backup completed."
${ECHO}
${ECHO} "Update contrib modules."
${ECHO}

drush up --no-core -y

${BASH} ${DRUPAL_ROOT_DEV}/sites/default/bin/dump-this-db.sh "d${VERSION}-post-contrib-update"
drush vset --exact maintenance_mode 0 -y
exit
