About
=====

Requires drush

Use these simple scripts to:
  - create a dump of your production database (production db needs to be on the same machine).
  - import the production database to your development database.
  - make a backup of your development database before/after making changes.
  - push database dumps to your git repo for import to staging during initial development.

Run these scripts from the "sites/default/bin" directory
e.g. ./dump-prod-db.sh

Files
======

config.cfg.dist
dump-prod-db.sh
dump-this-db.sh
.htaccess
import-prod2this.sh
README.txt
upgrade-core.sh
upgrade-no-core.sh

config.cfg.dist
===============

Copy or move this file to config.cfg
Edit the file with your production database credentials.
Add "sites/default/bin" to .gitignore.

dump-prod-db.sh
================

Dumps your production database to "sites/default/db".

dump-this-db.sh
================

Dumps the current development database.
Adds a description to the dump filename like so:

./dump-this-db.sh description-here-with-no-spaces

import-prod2this.sh
===================

Imports your production database to your development database.
NOTE: This destroys the previous version of the database.
Run "dump-this-db.sh" first. 

upgrade-no-core.sh
==================

Upgrades both files and database for contributed modules.
*NB* Run this script before upgrade-core.sh
Set variables at the top of the script.
upgrade-core.sh
===============

Upgrades Drupal core.
Set variables at the top of the script.

