#!/bin/bash

################
#
# dump-this-db.sh
# Dumps the database for this drupal install
# Run command as ./dump-this-db.sh descriptive-comment
# "descriptive-comment" will be added to filename. Comment should have no spaces.
#
#################
DATE=$(date +%y%m%d.%H.%M)
DBNAME=`drush sql-connect | awk '{print $2}' | cut -b 1-11 --complement`
BRANCH=`git rev-parse --abbrev-ref HEAD` # Alternatively can use: git branch | sed -n '/\* /s///p'

if [ -z $1 ]; then
  echo "Please add a description (no spaces) after the command e.g. ./dump-this-db.sh description-goes-here"
fi

drush  sql-dump | gzip > "../db/${DATE}_${DBNAME}_b${BRANCH}_$1.sql.gz"
exit
