#!/bin/bash

################
#
# dump-prod-db.sh
# Dumps prodution database
#
#
#################

if [ ! -f config.cfg ]; then
  echo ""
  echo "config.cfg is not found."
  echo "Copy or move config.cfg.dist to config.cfg and edit in the production database values."
  echo ""
  exit
elif [ -f config.cfg ]; then
. ./config.cfg
  if \
    [ -z $PROD_USER ]  ||\
    [ -z $PROD_PASSW ] ||\
    [ -z $PROD_DB ]    ||\
    [ -z $PROD_HOST ]; then
    echo ""
    echo "Please enter production database values in config.cfg."
    echo ""
    exit
  fi
fi

mysqldump  --user=${PROD_USER} --password=${PROD_PASSW} --host=${PROD_HOST}  ${PROD_DB} | gzip > ../db/${DATE}_${PROD_DB}.sql.gz
exit

